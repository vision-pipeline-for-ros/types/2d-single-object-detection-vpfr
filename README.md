![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# 2D Object detection VPFR
This is a type of vision pipeline.  
This type starts an instance of a vision pipeline.  
The instance then loads all algorithms that match the type.  
This type processes images and searches for an object in the image.  
If an object is found, it is written into the output topic and a preview is generated.  


Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/types/2d-single-object-detection-vpfr/-/wikis/home)


