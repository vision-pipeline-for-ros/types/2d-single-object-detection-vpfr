#!/usr/bin/env python3

"""
2d-object-detection
Copyright 2020 Vision Pipeline for ROS
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""

import sys
from sensor_msgs.msg import Image
from vpfr2dobjectdetection.msg import *
from vpfr import *


if __name__ == "__main__":

    if len(sys.argv) < 4:
        print(
            "usage: VpfrNode.py subscribechannel publishchannel AdditionalArgs"
        )
    else:
        VisionPipeline(
            "vpfr2dobjectdetection",
            sys.argv[1],
            Image,
            sys.argv[2],
            object2D,
            sys.argv,
        )
